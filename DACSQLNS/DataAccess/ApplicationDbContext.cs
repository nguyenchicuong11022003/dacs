﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DACSQLNS.Models;
public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options)
    {
    }
    public DbSet<Training> Training { get; set; }
    public DbSet<TrainingType> TrainingType { get; set; }
    public DbSet<Contract> Contract { get; set; }

    public DbSet<Payroll> Payrolls { get; set; }
    public DbSet<RewardDiscipline> RewardDiscipline { get; set; }
    public DbSet<Evaluation> Evaluation { get; set; }
    public DbSet<Asset> Asset { get; set; }
    public DbSet<LeaveRequest> LeaveRequests { get; set; }
    public DbSet<MyActivity> MyActivity { get; set; }
    public DbSet<Department> Department { get; set; }
    public DbSet<UserTraining> UserTraining { get; set; }
    public DbSet<Image> Images { get; set; }
 
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Cấu hình mối quan hệ giữa ApplicationUser và UserTraining
        modelBuilder.Entity<UserTraining>()
            .HasOne(ut => ut.User)
            .WithMany(u => u.UserTraining)
            .HasForeignKey(ut => ut.UserId);

        // Cấu hình mối quan hệ giữa Training và UserTraining
        modelBuilder.Entity<UserTraining>()
            .HasOne(ut => ut.Training)
            .WithMany(t => t.UserTraining)
            .HasForeignKey(ut => ut.TrainingId);

        // Đánh dấu UserTraining là không có khóa chính
        modelBuilder.Entity<UserTraining>().HasKey(x => new { x.UserId, x.TrainingId });
        

        // Gọi phương thức OnModelCreating từ lớp cha
        base.OnModelCreating(modelBuilder);
    }

}