﻿// Gửi yêu cầu xóa đối tượng và xử lý phản hồi từ server
$(".delete-link").click(function () {
    var id = $(this).data("id");
    $.ajax({
        type: "POST",
        url: "/Training/DeleteConfirmed",
        data: { id: id },
        success: function () {
            // Xóa đối tượng trên giao diện
            $("#item-" + id).remove();
        },
        error: function (xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
});

// Kiểm tra nếu có đối tượng đã bị xóa và ẩn nó khi tải lại trang
$("[id^='item-']").each(function () {
    var id = $(this).attr("id").replace("item-", "");
    if ($("#isDeleted-" + id).val() === "True") {
        $(this).hide();
    }
});
