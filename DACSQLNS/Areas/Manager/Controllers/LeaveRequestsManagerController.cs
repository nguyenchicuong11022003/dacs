﻿using DACSQLNS.Models;
using DACSQLNS.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;


namespace DACSQLNS.Areas.Manager.Controllers
{
    [Area("Manager")]
    [Authorize(Roles = SD.Role_Manager)]
    public class LeaveRequestsManagerController : Controller
    {
        private readonly ApplicationDbContext _context;
      


        public LeaveRequestsManagerController(ApplicationDbContext context)
        {
            _context = context;
            
        }

        public async Task<IActionResult> Index()
        {
            // Lấy danh sách tất cả các đơn xin nghỉ ở trạng thái đang xử lý
            var leaveRequests = await _context.LeaveRequests
                .Where(lr => lr.Status == "Đang chờ xử lý")
                .Include(lr => lr.ApplicationUser) // Đảm bảo rằng ApplicationUser đã được tải
                .ToListAsync();

            // Đếm số lượng đơn đang chờ xử lý và truyền qua ViewBag
            ViewBag.PendingApprovalCount = leaveRequests.Count;

            return View(leaveRequests);
        }


        public async Task<IActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveRequest = await _context.LeaveRequests.Include(lr => lr.ApplicationUser).FirstOrDefaultAsync(lr => lr.Id == id);
            if (leaveRequest == null)
            {
                return NotFound();
            }

            leaveRequest.Status = "Đã phê duyệt";
            leaveRequest.UpdatedDate = DateTime.Now;
            await _context.SaveChangesAsync();

            // Trích xuất thông tin từ đơn xin nghỉ phép đã được phê duyệt
            var employeeName = leaveRequest.ApplicationUser.FullName;
            var startDate = leaveRequest.StartTime.ToShortDateString();
            var endDate = leaveRequest.EndTime.ToShortDateString();
            var reason = leaveRequest.Reason;

            // Đường dẫn tới file template email HTML
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "ApprovedLeaveRequest.html");

            // Đọc nội dung của template email
            string emailContent;
            try
            {
                emailContent = System.IO.File.ReadAllText(templatePath);
            }
            catch (Exception ex)
            {
                return Content($"Error reading template: {ex.Message}");
            }

            // Thay thế các placeholder trong template email bằng thông tin tương ứng từ đơn xin nghỉ phép
            emailContent = emailContent.Replace("[Employee's Name]", employeeName)
                                       .Replace("[Start Date]", startDate)
                                       .Replace("[End Date]", endDate)
                                       .Replace("[Reason]", reason);

            // Kiểm tra xem các giá trị đã được thay thế đúng cách chưa
            System.Diagnostics.Debug.WriteLine($"Email Content: {emailContent}");

            // Gửi email thông báo
            var result = clsMail.Send(leaveRequest.ApplicationUser.Email, "Đơn Xin Nghỉ Phép Đã Được Phê Duyệt", emailContent, "ApprovedLeaveRequest");
            if (result != "OK")
            {
                // Trả về nội dung lỗi
                return Content($"Error sending email: {result}");
            }

            return RedirectToAction(nameof(Index));
        }



        // Action để từ chối đơn xin nghỉ
        public async Task<IActionResult> Reject(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveRequest = await _context.LeaveRequests.Include(lr => lr.ApplicationUser).FirstOrDefaultAsync(lr => lr.Id == id);
            if (leaveRequest == null)
            {
                return NotFound();
            }

            leaveRequest.Status = "Bị từ chối";
            leaveRequest.UpdatedDate = DateTime.Now;
            await _context.SaveChangesAsync();

            // Trích xuất thông tin từ đơn xin nghỉ phép đã bị từ chối
            var employeeName = leaveRequest.ApplicationUser.FullName;
            var startDate = leaveRequest.StartTime.ToShortDateString();
            var endDate = leaveRequest.EndTime.ToShortDateString();
            var reason = leaveRequest.Reason;

            // Đường dẫn tới file template email HTML
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "RejectedLeaveRequest.html");

            // Đọc nội dung của template email
            string emailContent;
            try
            {
                emailContent = System.IO.File.ReadAllText(templatePath);
            }
            catch (Exception ex)
            {
                return Content($"Error reading template: {ex.Message}");
            }

            // Thay thế các placeholder trong template email bằng thông tin tương ứng từ đơn xin nghỉ phép
            emailContent = emailContent.Replace("[Employee's Name]", employeeName)
                                       .Replace("[Start Date]", startDate)
                                       .Replace("[End Date]", endDate)
                                       .Replace("[Reason]", reason);

            // Gửi email thông báo
            var result = clsMail.Send(leaveRequest.ApplicationUser.Email, "Đơn Xin Nghỉ Phép Bị Từ Chối", emailContent, "RejectedLeaveRequest");
            if (result != "OK")
            {
                // Trả về nội dung lỗi
                return Content($"Error sending email: {result}");
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveRequest = await _context.LeaveRequests
                .Include(lr => lr.ApplicationUser)
                .FirstOrDefaultAsync(lr => lr.Id == id);

            if (leaveRequest == null)
            {
                return NotFound();
            }

            return View(leaveRequest);
        }
        public async Task<IActionResult> ApprovedRequests(int? month, int? year)
        {
            month ??= DateTime.Now.Month;
            year ??= DateTime.Now.Year;

            var approvedLeaveRequests = await _context.LeaveRequests
                .Where(lr => lr.Status == "Đã phê duyệt" && lr.StartTime.Month == month && lr.StartTime.Year == year)
                .Include(lr => lr.ApplicationUser)
                .ToListAsync();

            ViewBag.SelectedMonth = month;
            ViewBag.SelectedYear = year;

            return View(approvedLeaveRequests);
        }


        public async Task<IActionResult> RejectedRequests(int? month, int? year)
        {
            month ??= DateTime.Now.Month;
            year ??= DateTime.Now.Year;

            var rejectedLeaveRequests = await _context.LeaveRequests
                .Where(lr => lr.Status == "Bị từ chối" && lr.StartTime.Month == month && lr.StartTime.Year == year)
                .Include(lr => lr.ApplicationUser)
                .ToListAsync();

            ViewBag.SelectedMonth = month;
            ViewBag.SelectedYear = year;

            return View(rejectedLeaveRequests);
        }


    }
}
