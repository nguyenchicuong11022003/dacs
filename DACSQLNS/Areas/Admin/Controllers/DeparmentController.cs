﻿using DACSQLNS.Models;
using DACSQLNS.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DACSQLNS.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = SD.Role_Admin)]
	public class DeparmentController : Controller
    {
        private readonly IDeparmentReposity _deparmentRepository;
        private readonly ApplicationDbContext _context;

        public DeparmentController(IDeparmentReposity deparmentRepository, ApplicationDbContext context)
        {
            _deparmentRepository = deparmentRepository;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var activeDeparment = _context.Department.Where(t => !t.IsDeleted).ToList();
            return View(activeDeparment);
        }
        public async Task<IActionResult> RecycleBin()
        {
            var deleteDeparment = _context.Department.Where(d => d.IsDeleted).ToList();
            return View(deleteDeparment);
        }
        public async Task<IActionResult> Display(int id)
        {
            var deparment = await _deparmentRepository.GetByIdAsync(id);
            if (deparment == null)
            {
                return NotFound();
            }
            return View(deparment);
        }
        public async Task<IActionResult> Add()
        {
            var deparment = await _deparmentRepository.GetAllAsync();
            ViewBag.Department = new SelectList(deparment, "Id", "DepartmentName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(Department department)
        {
            if (ModelState.IsValid)
            {
                await _deparmentRepository.AddAsync(department);
                return RedirectToAction(nameof(Index));
            }
            var deparment = await _deparmentRepository.GetAllAsync();
            ViewBag.Department = new SelectList(deparment, "Id", "DepartmentName");
            return View(deparment);
        }
        public async Task<IActionResult> Update(int id, Department department)
        {
            if (id != department.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _deparmentRepository.UpdateAsync(department);
                return RedirectToAction(nameof(Index));
            }
            return View(department);
        }
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var department = await _deparmentRepository.GetByIdAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            // Thực hiện xóa mềm bằng cách đặt IsDeleted thành true
            department.IsDeleted = true;
            await _deparmentRepository.UpdateAsync(department);

            return RedirectToAction(nameof(Index));
        }
        [HttpPost, ActionName("Restore")]
        public async Task<IActionResult> Restore(int id)
        {
            var department = await _deparmentRepository.GetByIdAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            // Khôi phục đối tượng bằng cách đặt IsDeleted thành false
            department.IsDeleted = false;
            await _deparmentRepository.UpdateAsync(department);

            return Ok(); // Trả về thành công
        }
    }
}
