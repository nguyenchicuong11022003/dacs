﻿using DACSQLNS.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using DACSQLNS.Repositories;
using static DACSQLNS.Models.UserTraining;
using SQLitePCL;
using DACSQLNS.Services;

namespace DACSQLNS.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class UserTrainingManagerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UserTrainingManagerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // Trang hiển thị danh sách khóa đào tạo và các đăng ký cần duyệt
        public async Task<IActionResult> Index()
        {
            var trainings = await _context.Training
                .Include(t => t.UserTraining)
                .Where(t => !t.IsDeleted)
                .ToListAsync();

            return View(trainings);
        }

        // Trang hiển thị danh sách khóa đào tạo đã duyệt
        public async Task<IActionResult> IndexStatus()
        {
            var trainings = await _context.Training
                .Include(t => t.UserTraining)
                .Where(t => !t.IsDeleted)
                .ToListAsync();

            return View(trainings);
        }
        // Trang hiển thị danh sách khóa đào tạo đã đánh giá
        public async Task<IActionResult> IndexEvaluate()
        {
            var trainings = await _context.Training
                .Include(t => t.UserTraining)
                .Where(t => !t.IsDeleted)
                .ToListAsync();

            return View(trainings);
        }
        public async Task<IActionResult> ApprovedList(int id)
        {
            var training = await _context.Training.FindAsync(id);

            if (training == null)
            {
                return NotFound(); // Trả về trang 404 Not Found nếu không tìm thấy khóa đào tạo
            }

            var approvedTrainings = await _context.UserTraining
                .Where(ut => ut.Status == UserTraining.TrainingStatus.DaPheDuyet && ut.TrainingId == id)
                .Include(ut => ut.User)
                .ToListAsync();

            ViewBag.TrainingName = training.TrainingName;

            return View(approvedTrainings);
        }


        // Action method để hiển thị danh sách đăng ký bị từ chối
        public async Task<IActionResult> RejectedList(int id)
        {
            var training = await _context.Training.FindAsync(id);

            if (training == null)
            {
                return NotFound(); // Trả về trang 404 Not Found nếu không tìm thấy khóa đào tạo
            }
            var rejectedTrainings = await _context.UserTraining
                .Where(ut => ut.Status == UserTraining.TrainingStatus.DaTuChoi && ut.TrainingId == id)
                .Include(ut => ut.User)
                .ToListAsync();

            ViewBag.TrainingName = training.TrainingName;

            return View(rejectedTrainings);
        }

        // Trang hiển thị danh sách đăng ký của một khóa đào tạo cụ thể
        public async Task<IActionResult> RegistrationList(int id)
        {
            var usertraining = await _context.UserTraining
                .Include( ut => ut.User)
                .Where(ut => ut.TrainingId == id && ut.Status == UserTraining.TrainingStatus.ChuaXuLy)
                .ToListAsync();

            if (usertraining == null)
            {
                return NotFound();
            }

            return View(usertraining);
        }


        // Action method để xử lý duyệt đăng ký
        [HttpPost]
        public async Task<IActionResult> Approve(string userId, int trainingId)
        {
            // Lấy thông tin UserTraining cần duyệt
            var userTraining = await _context.UserTraining
                .Include(ut => ut.User)
                .Include(ut => ut.Training)
                .FirstOrDefaultAsync(ut => ut.UserId == userId && ut.TrainingId == trainingId);

            // Kiểm tra nếu không tìm thấy userTraining
            if (userTraining == null)
            {
                return NotFound("UserTraining not found.");
            }

            // Kiểm tra nếu không có thông tin người dùng
            if (userTraining.User == null)
            {
                return Content("Error: User not found.");
            }

            // Kiểm tra nếu không có thông tin khóa đào tạo
            if (userTraining.Training == null)
            {
                return Content("Error: Training not found.");
            }

            // Duyệt đăng ký
            userTraining.Status = UserTraining.TrainingStatus.DaPheDuyet;

            // Cập nhật vào cơ sở dữ liệu
            await _context.SaveChangesAsync();

            // Trích xuất thông tin từ khóa đào đã được phê duyệt
            var employeeName = userTraining.User.FullName;
            var trainingName = userTraining.Training.TrainingName;
            var startDate = userTraining.Training.TrainingStarTime.ToShortDateString();
            var endDate = userTraining.Training.TrainingEndTime.ToShortDateString();
            var content = userTraining.Training.TrainingContent;

            // Đường dẫn tới file template email HTML
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "TrainingRegistrationSuccess.html");

            // Đọc nội dung của template email
            string emailContent;
            try
            {
                emailContent = System.IO.File.ReadAllText(templatePath);
            }
            catch (Exception ex)
            {
                return Content($"Error reading template: {ex.Message}");
            }

            // Thay thế các placeholder trong template email bằng thông tin tương ứng
            emailContent = emailContent.Replace("[Employee's Name]", employeeName)
                                       .Replace("[Training Name]", trainingName)
                                       .Replace("[Start Date]", startDate)
                                       .Replace("[End Date]", endDate)
                                       .Replace("[Description]", content);

            // Ghi log nội dung email để kiểm tra
            System.Diagnostics.Debug.WriteLine($"Email Content: {emailContent}");

            // Gửi email thông báo
            var result = clsMail.Send(userTraining.User.Email, "Đăng ký khóa đào tạo thành công", emailContent, "TrainingRegistrationSuccess");
            if (result != "OK")
            {
                return Content($"Error sending email: {result}");
            }

            // Chuyển hướng về trang danh sách đăng ký
            return RedirectToAction(nameof(RegistrationList), new { id = trainingId });
        }


        [HttpPost]
        public async Task<IActionResult> Reject(string userId, int trainingId)
        {
            // Lấy thông tin UserTraining cần từ chối
            var userTraining = await _context.UserTraining
                .Include(ut => ut.User)
                .Include(ut => ut.Training)
                .FirstOrDefaultAsync(ut => ut.UserId == userId && ut.TrainingId == trainingId);

            // Kiểm tra nếu không tìm thấy userTraining
            if (userTraining == null)
            {
                return NotFound("UserTraining not found.");
            }

            // Kiểm tra nếu không có thông tin người dùng
            if (userTraining.User == null)
            {
                return Content("Error: User not found.");
            }

            // Kiểm tra nếu không có thông tin khóa đào tạo
            if (userTraining.Training == null)
            {
                return Content("Error: Training not found.");
            }

            // Thực hiện các thao tác xử lý khi từ chối đăng ký
            userTraining.Status = TrainingStatus.DaTuChoi;

            // Lưu thay đổi vào cơ sở dữ liệu
            await _context.SaveChangesAsync();

            // Trích xuất thông tin từ khóa đào tạo đã bị từ chối
            var employeeName = userTraining.User.FullName;
            var trainingName = userTraining.Training.TrainingName;
            var startDate = userTraining.Training.TrainingStarTime.ToShortDateString();
            var endDate = userTraining.Training.TrainingEndTime.ToShortDateString();
            var content = userTraining.Training.TrainingContent;

            // Đường dẫn tới file template email HTML
            var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "TrainingRegistrationFailure.html");

            // Đọc nội dung của template email
            string emailContent;
            try
            {
                emailContent = System.IO.File.ReadAllText(templatePath);
            }
            catch (Exception ex)
            {
                return Content($"Error reading template: {ex.Message}");
            }

            // Thay thế các placeholder trong template email bằng thông tin tương ứng
            emailContent = emailContent.Replace("[Employee's Name]", employeeName)
                                       .Replace("[Training Name]", trainingName)
                                       .Replace("[Start Date]", startDate)
                                       .Replace("[End Date]", endDate)
                                       .Replace("[Description]", content);

            // Ghi log nội dung email để kiểm tra
            System.Diagnostics.Debug.WriteLine($"Email Content: {emailContent}");

            // Gửi email thông báo
            var result = clsMail.Send(userTraining.User.Email, "Đăng ký khóa đào tạo thất bại", emailContent, "TrainingRegistrationFailure");
            if (result != "OK")
            {
                return Content($"Error sending email: {result}");
            }

            // Chuyển hướng về trang danh sách đăng ký
            return RedirectToAction(nameof(RegistrationList), new { id = trainingId });
        }



        //==============================================================================
        // Action method để hiển thị trang đánh giá cho một khóa đào tạo cụ thể
        public async Task<IActionResult> Evaluate(int id)
        {
            var userTrainings = await _context.UserTraining
                .Include(ut => ut.User)
                .Where(ut => ut.TrainingId == id && ut.Status == UserTraining.TrainingStatus.DaPheDuyet)
                .ToListAsync();

            if (userTrainings == null)
            {
                return NotFound();
            }

            return View(userTrainings);
        }
        // Action method để xử lý đánh giá hoàn thành
        [HttpPost]
        public async Task<IActionResult> CompleteEvaluation(string userId, int trainingId)
        {
            var userTraining = await _context.UserTraining
                .FirstOrDefaultAsync(ut => ut.UserId == userId && ut.TrainingId == trainingId);

            if (userTraining == null)
            {
                return NotFound();
            }

            userTraining.Completed = true;
            userTraining.CompletedAt = DateTime.Now;

            _context.Update(userTraining);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Evaluate), new { id = trainingId });
        }

        // Action method để xử lý đánh giá chưa hoàn thành
        [HttpPost]
        public async Task<IActionResult> IncompleteEvaluation(string userId, int trainingId)
        {
            var userTraining = await _context.UserTraining
                .FirstOrDefaultAsync(ut => ut.UserId == userId && ut.TrainingId == trainingId);

            if (userTraining == null)
            {
                return NotFound();
            }

            userTraining.Completed = false;
            userTraining.CompletedAt = DateTime.Now;

            _context.Update(userTraining);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Evaluate), new { id = trainingId });
        }
        public async Task<IActionResult> CompletedEvaluationList(int id)
        {
            var completedTrainings = await _context.UserTraining
                .Where(ut => ut.Completed == true && ut.CompletedAt != null && ut.TrainingId == id)
                .Include(ut => ut.User)
                .Include(ut => ut.Training)
                .ToListAsync();

            return View(completedTrainings);
        }

        public async Task<IActionResult> IncompleteEvaluationList(int id)
        {
            var incompleteTrainings = await _context.UserTraining
                .Where(ut => ut.Completed == false && ut.CompletedAt != null && ut.TrainingId == id)
                .Include(ut => ut.User)
                .Include(ut => ut.Training)
                .ToListAsync();

            return View(incompleteTrainings);
        }


    }
}
