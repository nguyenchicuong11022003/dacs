﻿using DACSQLNS.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using DACSQLNS.Repositories;

namespace DACSQLNS.Areas.Admin.Controllers
{

    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class OwnerController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IDeparmentReposity _deparmentReposity;
        private readonly ApplicationDbContext _context;

        public OwnerController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IDeparmentReposity deparmentReposity)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _deparmentReposity = deparmentReposity;
        }



        public async Task<IActionResult> Index()
        {
            var currenUser = await _userManager.GetUserAsync(User);
            var userRoles = await _userManager.GetRolesAsync(currenUser);
            var users = await _userManager.Users.Where(u => u.Conpany == currenUser.Conpany  ).ToListAsync();
            return View(users);
        }
        [HttpGet]
        
        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            // Lấy danh sách các phòng ban
            var departments = _context.Department.Where(d => d.IsDeleted == false).ToList();

            // Tạo SelectList chứa danh sách các phòng ban
            ViewBag.Department = new SelectList(departments, "Id", "DepartmentName");
            // Lấy danh sách các vai trò
            var roles = await _roleManager.Roles.ToListAsync();

            // Lấy vai trò của người dùng
            var userRoles = await _userManager.GetRolesAsync(user);

            // Truyền danh sách vai trò vào ViewBag
            ViewBag.Roles = new SelectList(roles, "Name", "Name", userRoles);

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, string roleName, ApplicationUser updatedUser)
        {
            if (id != updatedUser.Id)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
     
            if (user == null)
            {
                return NotFound();
            }
            if (string.IsNullOrEmpty(roleName))
            {
                // Nếu không có vai trò nào được chọn, redirect lại trang Edit với thông báo lỗi
                ModelState.AddModelError("", "Please select a role.");
                return View(user);
            }

            // Cập nhật thông tin người dùng
            user.UserName = updatedUser.UserName;
            user.Email = updatedUser.Email;
            user.FullName = updatedUser.FullName;
            user.DepartmentId = updatedUser.DepartmentId;
            // Lấy vai trò hiện tại của người dùng
            var userRoles = await _userManager.GetRolesAsync(user);
       
            // Thêm vai trò mới cho người dùng và loại bỏ các vai trò cũ
            await _userManager.RemoveFromRolesAsync(user, userRoles.ToArray());
            await _userManager.AddToRoleAsync(user, roleName);
            //var a = await _userManager.GetRolesAsync(user);

            // Cập nhật thông tin người dùng vào cơ sở dữ liệu
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                // Xử lý lỗi nếu có
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
                return View(user);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]


        public IActionResult Add()
        {
            // Lấy danh sách các vai trò
            var roles = _roleManager.Roles.ToList();

            // Lấy danh sách các phòng ban
            var departments = _context.Department.Where(d => d.IsDeleted == false).ToList();

            // Tạo SelectList chứa danh sách các phòng ban
            ViewBag.Department = new SelectList(departments, "Id", "DepartmentName");

            // Truyền danh sách vai trò vào ViewBag
            ViewBag.Roles = new SelectList(roles, "Name", "Name");

            return View();
        }


        [HttpPost]
       
        public async Task<IActionResult> Add(ApplicationUser user, string roleName, string password)
        {
            // Lấy thông tin người dùng hiện tại
            var currentUser = await _userManager.GetUserAsync(User);

            // Kiểm tra nếu người dùng hiện tại không tồn tại
            if (currentUser == null)
            {
                return NotFound();
            }

            // Gán CompanyId cho người dùng mới là giống với CompanyId của người tạo
            user.Conpany = currentUser.Conpany;

            var result = await _userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                user.EmailConfirmed = true;
                await _userManager.AddToRoleAsync(user, roleName);
                return RedirectToAction(nameof(Index));
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            // Lấy lại danh sách các vai trò nếu có lỗi
            var roles = _roleManager.Roles.ToList();
 
            ViewBag.Roles = new SelectList(roles, "Name", "Name");

            return View(user);
        }


        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
          
            user.IsDelete = true;
         
            await _userManager.UpdateAsync(user);
            return RedirectToAction(nameof(Index));
        }
        [ActionName("RecycleBin")]
        public async Task<IActionResult> RecycleBin()
        {
            var userDelete = await _userManager.GetUserAsync(User);
            var deletedUsers = await _userManager.Users.Where(u => u.IsDelete == true && u.Conpany == userDelete.Conpany).ToListAsync();
            return View(deletedUsers);
        }

        [HttpPost, ActionName("Restore")]
        public async Task<IActionResult> Restore(string id)
        {
            var RestoreUser = await _userManager.FindByIdAsync(id);
            
                RestoreUser.IsDelete = false;
                await _userManager.UpdateAsync(RestoreUser);
            
            return Ok();
        }
   /*     [HttpPost, ActionName("GetId")]
         public async Task<IActionResult> GetIdUser(string id)
        {
            var userIDs = await _userManager.FindByIdAsync(id);
            return userIDs;
        }*/
    }
}
