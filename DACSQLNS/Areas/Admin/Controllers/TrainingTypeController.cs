﻿using DACSQLNS.Models;
using DACSQLNS.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DACSQLNS.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = SD.Role_Admin)]
	public class TrainingTypeController : Controller
    {
        private readonly ITrainingTypeRepository _trainingTypeRepository;
        private readonly ApplicationDbContext _context;
        public TrainingTypeController(ITrainingTypeRepository trainingTypeRepository, ApplicationDbContext context)
        {
            _trainingTypeRepository = trainingTypeRepository;
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var activeTrainings = _context.TrainingType.Where(t => !t.IsDeleted).ToList();
            return View(activeTrainings);
        }
        public async Task<IActionResult> RecycleBin()
        {
            var deletetrainingtype = _context.TrainingType.Where(d => d.IsDeleted).ToList();
            return View(deletetrainingtype);
        }
        public async Task<IActionResult> Display(int id)
        {
            var trainingtype = await _trainingTypeRepository.GetByIdAsync(id);
            if (trainingtype == null)
            {
                return NotFound();
            }
            return View(trainingtype);
        }
        public async Task<IActionResult> Add()
        {
            var trainingtype = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtype, "Id", "TrainingTypeName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(TrainingType trainingType)
        {
            if (ModelState.IsValid)
            {
                await _trainingTypeRepository.AddAsync(trainingType);
                return RedirectToAction(nameof(Index));
            }
            var trainingtype = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtype, "Id", "TrainingTypeName");
            return View(trainingtype);
        }
        public async Task<IActionResult> Update(int id, TrainingType trainingType)
        {
            if (id != trainingType.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _trainingTypeRepository.UpdateAsync(trainingType);
                return RedirectToAction(nameof(Index));
            }
            return View(trainingType);
        }
        public async Task<IActionResult> Delete(int id)
        {
            var trainingtype = await _trainingTypeRepository.GetByIdAsync(id);
            if (trainingtype == null)
            {
                return NotFound();
            }
            return View(trainingtype);
        }
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var training = await _trainingTypeRepository.GetByIdAsync(id);
            if (training == null)
            {
                return NotFound();
            }

            // Thực hiện xóa mềm bằng cách đặt IsDeleted thành true
            training.IsDeleted = true;
            await _trainingTypeRepository.UpdateAsync(training);

            return RedirectToAction(nameof(Index));
        }
        [HttpPost, ActionName("Restore")]
        public async Task<IActionResult> Restore(int id)
        {
            var trainingtype = await _trainingTypeRepository.GetByIdAsync(id);
            if (trainingtype == null)
            {
                return NotFound();
            }

            // Khôi phục đối tượng bằng cách đặt IsDeleted thành false
            trainingtype.IsDeleted = false;
            await _trainingTypeRepository.UpdateAsync(trainingtype);

            return Ok(); // Trả về thành công
        }

    }
}
