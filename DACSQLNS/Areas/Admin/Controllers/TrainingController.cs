﻿using DACSQLNS.Models;
using DACSQLNS.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
namespace DACSQLNS.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class TrainingController : Controller
    {

        private readonly ITrainingRepository _trainingRepository;
        private readonly ITrainingTypeRepository _trainingTypeRepository;
        private readonly IDeparmentReposity _deparmentReposity;
        private readonly ApplicationDbContext _context;

        public TrainingController(ITrainingRepository trainingRepository, ITrainingTypeRepository trainingTypeRepository, ApplicationDbContext context, IDeparmentReposity deparmentReposity)
        {
            _trainingRepository = trainingRepository;
            _trainingTypeRepository = trainingTypeRepository;
            _deparmentReposity = deparmentReposity;
            _context = context;
        }
        // Hiển thị Index
        public async Task<IActionResult> Index()
        {
            var activeTrainings = _context.Training.Where(t => !t.IsDeleted).ToList();
            var trainingtype = await _trainingTypeRepository.GetAllAsync();
            var department = await _deparmentReposity.GetAllAsync();
            return View(activeTrainings);
        }
        public async Task<IActionResult> RecycleBin()
        {
            var deletetraining = _context.Training.Where(d => d.IsDeleted).ToList();
            var trainingtype = await _trainingTypeRepository.GetAllAsync();
            return View(deletetraining);
        }
        //Hiển thị form thêm đào tạo
        public async Task<IActionResult> Add()
        {
            var department = await _deparmentReposity.GetAllAsync();
            // Tạo danh sách các SelectListItem từ danh sách phòng ban
            var departmentList = department
                .Select(d => new SelectListItem
                {
                    Value = d.Id.ToString(), // Chuyển đổi giá trị ID sang chuỗi
                    Text = d.DepartmentName // Tên phòng ban sẽ hiển thị trong dropdown
                }).ToList();

            // Thêm một mục rỗng vào đầu danh sách
            departmentList.Insert(0, new SelectListItem { Value = null, Text = " " });

            // Truyền danh sách đã được cập nhật vào ViewBag
            ViewBag.Department = departmentList;

            var trainingtypes = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtypes, "Id", "TrainingTypeName");

            return View();
        }
        // xử lý thêm đạo tạo mới
        [HttpPost]
        public async Task<IActionResult> Add(Training training)
        {
            if (ModelState.IsValid)
            {
                await _trainingRepository.AddAsync(training);
                return RedirectToAction(nameof(Index));
            }
            // nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập 
            var department = await _deparmentReposity.GetAllAsync();
            ViewBag.Department = new SelectList(department, "Id", "DepartmentName");
            var trainingtypes = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtypes, "Id", "TrainingTypeName");
            return View(training);
        }
        //Hiển thị thông tin chi tiết sản phẩm
        public async Task<IActionResult> Display(int id)
        {
            var training = await _trainingRepository.GetByIdAsync(id);
            var department = await _deparmentReposity.GetAllAsync();
            if (training == null)
            {
                return NotFound();
            }
            return View(training);
        }
        //Hien thi form cap nhat san pham
        public async Task<IActionResult> Update(int id)
        {
            var training = await _trainingRepository.GetByIdAsync(id);
            if (training == null)
            {
                return NotFound();
            }
            var department = await _deparmentReposity.GetAllAsync();
            // Tạo danh sách các SelectListItem từ danh sách phòng ban
            var departmentList = department
                .Select(d => new SelectListItem
                {
                    Value = d.Id.ToString(), // Chuyển đổi giá trị ID sang chuỗi
                    Text = d.DepartmentName // Tên phòng ban sẽ hiển thị trong dropdown
                }).ToList();

            // Thêm một mục rỗng vào đầu danh sách
            departmentList.Insert(0, new SelectListItem { Value = null, Text = " " });

            // Truyền danh sách đã được cập nhật vào ViewBag
            ViewBag.Department = departmentList;

            var trainingtypes = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtypes, "Id", "TrainingTypeName", training.TrainingTypeId);
            return View(training);
        }
        // xu ly cap nhat san pham
        [HttpPost]
        public async Task<IActionResult> Update(int id, Training training)
        {
            if (id != training.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var existingTraining = await _trainingRepository.GetByIdAsync(id);
                //cap nhat cac thong tin khac 
                existingTraining.TrainingName = training.TrainingName;
                existingTraining.TrainingContent = training.TrainingContent;
                existingTraining.TrainingStarTime = training.TrainingStarTime;
                existingTraining.TrainingEndTime = training.TrainingEndTime;
                existingTraining.TrainingTypeId = training.TrainingTypeId;
                existingTraining.DepartmentId = training.DepartmentId;

                await _trainingRepository.UpdateAsync(existingTraining);

                return RedirectToAction(nameof(Index));
            }
            var department = await _deparmentReposity.GetAllAsync();
            // Tạo danh sách các SelectListItem từ danh sách phòng ban
            var departmentList = department
                .Select(d => new SelectListItem
                {
                    Value = d.Id.ToString(), // Chuyển đổi giá trị ID sang chuỗi
                    Text = d.DepartmentName // Tên phòng ban sẽ hiển thị trong dropdown
                }).ToList();

            // Thêm một mục rỗng vào đầu danh sách
            departmentList.Insert(0, new SelectListItem { Value = null, Text = " " });

            // Truyền danh sách đã được cập nhật vào ViewBag
            ViewBag.Department = departmentList;

            var trainingtypes = await _trainingTypeRepository.GetAllAsync();
            ViewBag.TrainingType = new SelectList(trainingtypes, "Id", "TrainingTypeName");
            return View(training);
        }
        public async Task<IActionResult> Delete(int id)
        {
            var training = await _trainingRepository.GetByIdAsync(id);
            if (training == null)
            {
                return NotFound();
            }
            return View(training);
        }
        // xu ly xoa san pham
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var training = await _trainingRepository.GetByIdAsync(id);
            if (training == null)
            {
                return NotFound();
            }

            // Thực hiện xóa mềm bằng cách đặt IsDeleted thành true
            training.IsDeleted = true;
            await _trainingRepository.UpdateAsync(training);

            return RedirectToAction(nameof(Index));
        }
        [HttpPost, ActionName("Restore")]
        public async Task<IActionResult> Restore(int id)
        {
            var training = await _trainingRepository.GetByIdAsync(id);
            if (training == null)
            {
                return NotFound();
            }

            // Khôi phục đối tượng bằng cách đặt IsDeleted thành false
            training.IsDeleted = false;
            await _trainingRepository.UpdateAsync(training);

            return Ok(); // Trả về thành công
        }


    }
}
