﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DACSQLNS.Models;
using Microsoft.AspNetCore.Identity;

namespace DACSQLNS.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RewardDisciplinesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public RewardDisciplinesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Admin/RewardDisciplines
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = await _context.RewardDiscipline
                .Where(r => r.isStatus == false)
                .Include(r => r.ApplicationUser)
                .ToListAsync();
            return View(applicationDbContext);
        }


        // GET: Admin/RewardDisciplines/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewardDiscipline = await _context.RewardDiscipline
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rewardDiscipline == null)
            {
                return NotFound();
            }

            return View(rewardDiscipline);
        }

        // GET: Admin/RewardDisciplines/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "UserName");
            return View();
        }

        // POST: Admin/RewardDisciplines/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Reason,RewardDisciplineDate,RewardDisciplineDescription,UserId")] RewardDiscipline rewardDiscipline)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rewardDiscipline);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", rewardDiscipline.UserId);
            return View(rewardDiscipline);
        }

        // GET: Admin/RewardDisciplines/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewardDiscipline = await _context.RewardDiscipline.FindAsync(id);
            if (rewardDiscipline == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", rewardDiscipline.UserId);
            return View(rewardDiscipline);
        }

        // POST: Admin/RewardDisciplines/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Reason,RewardDisciplineDate,RewardDisciplineDescription,UserId")] RewardDiscipline rewardDiscipline)
        {
            if (id != rewardDiscipline.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rewardDiscipline);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RewardDisciplineExists(rewardDiscipline.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", rewardDiscipline.UserId);
            return View(rewardDiscipline);
        }

        // GET: Admin/RewardDisciplines/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rewardDiscipline = await _context.RewardDiscipline
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rewardDiscipline == null)
            {
                return NotFound();
            }

            return View(rewardDiscipline);
        }

        // POST: Admin/RewardDisciplines/Delete/5
        // POST: Admin/RewardDisciplines/Delete/5
      

        private bool RewardDisciplineExists(int id)
        {
            return _context.RewardDiscipline.Any(e => e.Id == id);
        }

     
        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reward = await _context.RewardDiscipline.FindAsync(id);
            if (reward == null)
            {
                return NotFound();
            }

            // Thực hiện xóa mềm bằng cách đặt IsDeleted thành true
            reward.isStatus = true;
           _context.RewardDiscipline.Update(reward);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
        [HttpPost, ActionName("Restore")]
        public async Task<IActionResult> Restore(int id)
        {
            var reward = await _context.RewardDiscipline.FindAsync(id);
            if (reward == null)
            {
                return NotFound();
            }

            // Khôi phục đối tượng bằng cách đặt IsDeleted thành false
            reward.isStatus = false;
            _context.RewardDiscipline.Update(reward);
            await _context.SaveChangesAsync();
            return Ok(); // Trả về thành công
        }
        [ ActionName("RecycleBin")]
        public async Task<IActionResult> RecycleBin()
        {
            var applicationDbContext = await _context.RewardDiscipline
               .Where(r => r.isStatus == true)
               .Include(r => r.ApplicationUser)
               .ToListAsync();
            return View(applicationDbContext);
        }
    }
}
