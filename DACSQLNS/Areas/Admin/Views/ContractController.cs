﻿using DACSQLNS.Models;
using DACSQLNS.Repositories;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Controllers
{
    public class ContractController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IDContractRepository _contractRepository;
        public ContractController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IDContractRepository contractRepository)
        {
            _context = context;
            _userManager = userManager;
            _contractRepository = contractRepository;
        }
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var contract = await _contractRepository.GetAllAsync();

            return View(contract);
        }
        public async Task<IActionResult> Add()
        {
            var userTT = await _userManager.GetUserAsync(User);
            var users = await _userManager.Users.Where(t => t.Conpany == userTT.Id && t.IsDelete == false && t.Id != t.Conpany).ToListAsync();
            ViewBag.Users = new SelectList(users, "Id", "UserName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(Contract contract)
        {
            // Lấy UserId tương ứng với UserName từ cơ sở dữ liệu
            var user = await _userManager.FindByNameAsync(contract.UserId);
            if (user != null)
            {
                contract.UserId = user.Id; // Gán UserId từ user tìm được
                await _contractRepository.AddAsync(contract);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                // Xử lý nếu không tìm thấy user
                ModelState.AddModelError("UserId", "User not found");
                var users = await _userManager.Users.ToListAsync();
                ViewBag.Users = new SelectList(users, "UserName", "UserName");
                return View(contract);
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            var contract = await _contractRepository.GetByIdAsync(id);
            if (contract == null)
            {
                return NotFound();
            }

            var userTT = await _userManager.GetUserAsync(User);
            var users = await _userManager.Users.Where(t => t.Conpany == userTT.Id && t.IsDelete == false && t.Id != t.Conpany).ToListAsync();
            ViewBag.Users = new SelectList(users, "Id", "UserName", contract.UserId);

            return View(contract);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Contract contract)
        {
            if (id != contract.Id)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(contract.UserId);
            if (user != null)
            {
                contract.UserId = user.Id; // Gán UserId từ user tìm được
            }
            else
            {
                ModelState.AddModelError("UserId", "User not found");
                var users = await _userManager.Users.ToListAsync();
                ViewBag.Users = new SelectList(users, "UserName", "UserName", contract.UserId);
                return View(contract);
            }

            await _contractRepository.UpdateAsync(contract);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Detail(int id)
        {
            var contract = await _contractRepository.GetByIdAsync(id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

    }
}