﻿using DACSQLNS.Models;
using DACSQLNS.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Controllers
{
    public class PayRollController : Controller
    {
        private readonly IDContractRepository _contractRepository;
        private readonly IDPayRollRepository _payrollRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public PayRollController(IDContractRepository contractRepository, IDPayRollRepository payrollRepository, UserManager<ApplicationUser> userManager)
        {
            _contractRepository = contractRepository;
            _payrollRepository = payrollRepository;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            var payrolls = await _payrollRepository.GetAllAsync();
            return View(payrolls);
        }
        public async Task<IActionResult> Add()
        {
            var user = await _userManager.GetUserAsync(User);
            var contracts = await _contractRepository.GetAllAsync();
            var userContracts = contracts.ToList();

            if (userContracts != null && userContracts.Any())
            {
                ViewBag.Contracts = new SelectList(userContracts, "Id", "Id");
            }
            else
            {
                ViewBag.Contracts = new SelectList(new List<Contract>(), "Id", "Id"); // Tạo SelectList rỗng
            }

            ViewBag.Users = new SelectList(await _userManager.Users.ToListAsync(), "Id", "UserName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(Payroll payroll)
        {
            if (ModelState.IsValid)
            {
                // Lấy Contract từ cơ sở dữ liệu để lấy BasicSalary
                var contract = await _contractRepository.GetByIdAsync(payroll.ContractId);
                if (contract != null)
                {
                    payroll.BasicSalary = contract.ContractSalary; // Gán BasicSalary từ Contract tìm được
                    await _payrollRepository.AddAsync(payroll);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    // Xử lý nếu không tìm thấy Contract
                    ModelState.AddModelError("ContractId", "Contract not found");
                    var contracts = await _contractRepository.GetAllAsync();
                    ViewBag.Contracts = new SelectList(contracts, "Id", "Id");
                    ViewBag.Users = new SelectList(await _userManager.Users.ToListAsync(), "Id", "UserName");
                    return View(payroll);
                }
            }
            var contractsAll = await _contractRepository.GetAllAsync();
            ViewBag.Contracts = new SelectList(contractsAll, "Id", "Id");
            ViewBag.Users = new SelectList(await _userManager.Users.ToListAsync(), "Id", "UserName");
            return View(payroll);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payroll = await _payrollRepository.GetByIdAsync(id.Value);
            if (payroll == null)
            {
                return NotFound();
            }

            return View(payroll);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payroll = await _payrollRepository.GetByIdAsync(id.Value);
            if (payroll == null)
            {
                return NotFound();
            }

            var contracts = await _contractRepository.GetAllAsync();
            ViewBag.Contracts = new SelectList(contracts, "Id", "Id", payroll.ContractId);
            return View(payroll);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Payroll payroll)
        {
            if (id != payroll.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _payrollRepository.UpdateAsync(payroll);
                }
                catch (DbUpdateConcurrencyException)
                {
                   /* if (!PayrollExists(payroll.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }*/
                }
                return RedirectToAction(nameof(Index));
            }

            var contracts = await _contractRepository.GetAllAsync();
            ViewBag.Contracts = new SelectList(contracts, "Id", "Id", payroll.ContractId);
            return View(payroll);
        }

     

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var payroll = await _payrollRepository.GetByIdAsync(id.Value);
            if (payroll == null)
            {
                return NotFound();
            }

            return View(payroll);
        }

        /*  [HttpPost, ActionName("Delete")]
          public async Task<IActionResult> DeleteConfirmed(int id)
          {
              var payroll = await _payrollRepository.GetByIdAsync(id);
              await _payrollRepository.DeleteAsync(payroll);
              return RedirectToAction(nameof(Index));
          }

          private bool PayrollExists(int id)
          {
              return _payrollRepository.GetByIdAsync(id) != null;
          }*/
        [HttpPost]
        public async Task<decimal> GetBasicSalary(int contractId)
        {
            var contract = await _contractRepository.GetByIdAsync(contractId);
            if (contract != null)
            {
                return contract.ContractSalary;
            }
            return 0;
        }
    }
}
