﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACSQLNS.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Globalization;

namespace DACSQLNS.Controllers
{
    public class LeaveRequestsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public LeaveRequestsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            // Lấy UserId của người dùng hiện tại
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            // Lấy danh sách các đơn xin nghỉ của người dùng đó ở trạng thái đang xử lý
            var leaveRequests = await _context.LeaveRequests
                .Where(lr => lr.UserId == userId && lr.Status == "Đang chờ xử lý")
                .ToListAsync();

            return View(leaveRequests);
        }



        public IActionResult Create()
        {
            // Tạo một đối tượng LeaveRequest mới
            var leaveRequest = new LeaveRequest();

            // Thiết lập UserId từ thông tin người dùng hiện tại
            leaveRequest.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            return View(leaveRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LeaveRequest leaveRequest, IFormFile evidenceFile)
        {
            if (evidenceFile == null || evidenceFile.Length == 0)
            {
                ModelState.AddModelError("evidenceFile", "Evidence file is required.");
            }
            if (ModelState.IsValid)
            {
              
                if (evidenceFile != null && evidenceFile.Length > 0)
                {
                    // Lưu hình ảnh minh chứng
                    leaveRequest.ImageUrl = await SaveImage(evidenceFile);
                }
                leaveRequest.CreatedDate = DateTime.Now;
                leaveRequest.Status = "Đang chờ xử lý";
                await _context.LeaveRequests.AddAsync(leaveRequest);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            // Nếu ModelState không hợp lệ, hiển thị lại form và thông báo lỗi
            ViewBag.ErrorMessage = "Please upload the evidence file.";
            return View(leaveRequest);
        }
        private async Task<string> SaveImage(IFormFile image)
        {
            var savePath = Path.Combine("wwwroot/images", image.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            return "/images/" + image.FileName; // Trả về đường dẫn tương đối
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveRequest = await _context.LeaveRequests
                .Include(lr => lr.ApplicationUser)
                .FirstOrDefaultAsync(lr => lr.Id == id);

            if (leaveRequest == null)
            {
                return NotFound();
            }

            return View(leaveRequest);
        }
        public async Task<IActionResult> History(int? month, int? year)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            month ??= DateTime.Now.Month;
            year ??= DateTime.Now.Year;

            var leaveRequests = await _context.LeaveRequests
                .Where(lr => lr.UserId == userId && lr.Status != "Đang chờ xử lý" && lr.StartTime.Month == month && lr.StartTime.Year == year)
                .ToListAsync();

            var approvedLeaveRequests = leaveRequests
                .Where(lr => lr.Status == "Đã phê duyệt");

            int totalDaysOff = approvedLeaveRequests
                .Sum(lr => lr.DaysOff);

            ViewBag.TotalDaysOff = totalDaysOff;
            ViewBag.SelectedMonth = month;
            ViewBag.SelectedYear = year;

            return View(leaveRequests);
        }
         public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var leaveRequest = await _context.LeaveRequests
            .FirstOrDefaultAsync(m => m.Id == id);
        if (leaveRequest == null)
        {
            return NotFound();
        }

        return View(leaveRequest);
    }

    // POST: LeaveRequests/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var leaveRequest = await _context.LeaveRequests.FindAsync(id);
        if (leaveRequest != null)
        {
            _context.LeaveRequests.Remove(leaveRequest);
            await _context.SaveChangesAsync();
        }

        return RedirectToAction(nameof(Index));
    }


    }
}
