﻿
using DACSQLNS.Models;
using Microsoft.AspNetCore.Mvc;
using static DACSQLNS.Models.UserTraining;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using DACSQLNS.Repositories;

[Authorize]
public class UserTrainingController : Controller
{
    private readonly ApplicationDbContext _context;
    private readonly ITrainingRepository _trainingRepository;
    private readonly IDeparmentReposity _deparmentReposity;

    public UserTrainingController(ApplicationDbContext context, ITrainingRepository trainingRepository, IDeparmentReposity deparmentReposity)
    {
        _context = context;
        _trainingRepository = trainingRepository;
        _deparmentReposity = deparmentReposity;
    }

    // Trang hiển thị các khóa đào tạo để đăng ký
    public async Task<IActionResult> Index(string searchString)
    {
        // Lấy Id của người dùng hiện tại
        string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

        // Lấy danh sách các khóa đào tạo mà người dùng đã đăng ký
        var userTrainings = await _context.UserTraining
                                            .Where(ut => ut.UserId == userId)
                                            .Select(ut => ut.TrainingId)
                                            .ToListAsync();

        // Lấy danh sách các khóa đào tạo chưa được đăng ký bởi người dùng
        IQueryable<Training> trainings = _context.Training
                                                .Where(t => !t.IsDeleted && !userTrainings.Contains(t.Id));

        // Kiểm tra xem có từ khóa tìm kiếm không
        if (!string.IsNullOrEmpty(searchString))
        {
            // Tìm kiếm theo tên khóa đào tạo, tên phòng và nội dung của khóa đào tạo
            trainings = trainings.Where(t =>
                t.TrainingName.Contains(searchString) ||
                t.Department.DepartmentName.Contains(searchString) ||
                t.TrainingContent.Contains(searchString)
            );
        }

        // Lưu từ khóa tìm kiếm vào ViewBag
        ViewBag.SearchString = searchString;

        // Hiển thị kết quả tìm kiếm hoặc thông báo không có kết quả
        if (trainings.Any())
        {
            return View(await trainings.ToListAsync());
        }
        else
        {
            ViewBag.Message = "Không có kết quả.";
            return View(new List<Training>()); // Trả về một danh sách rỗng nếu không có kết quả
        }
    }


    // Trang hiển thị tình trạng đăng ký
    public IActionResult RegistrationStatus()
    {
        // Lấy thông tin đăng ký của người dùng từ cơ sở dữ liệu
        // và truyền vào view
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        var userTrainings = _context.UserTraining
            .Where(ut => ut.UserId == userId)
            .Include(ut => ut.Training)
            .ToList();
        return View(userTrainings);
    }

    // Trang hiển thị kết quả
    public IActionResult Results()
    {
        // Lấy thông tin kết quả của người dùng từ cơ sở dữ liệu
        // và truyền vào view
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        var userTrainings = _context.UserTraining
            .Where(ut => ut.UserId == userId)
            .Include(ut => ut.Training)
            .ToList();
        return View(userTrainings);
    }

    // Action method để xử lý đăng ký
    [HttpPost]
    public IActionResult Register(int trainingId)
    {
        // Lấy thông tin người dùng
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        // Tạo đối tượng UserTraining mới
        var userTraining = new UserTraining
        {
            UserId = userId,
            TrainingId = trainingId,
            Status = TrainingStatus.ChuaXuLy, // Mặc định là chưa xử lý
            Completed = false, // Mặc định là chưa hoàn thành
            CreatedAt = DateTime.Now // Thời gian đăng ký
        };
        // Lưu vào cơ sở dữ liệu
        _context.UserTraining.Add(userTraining);
        _context.SaveChanges();
        // Chuyển hướng về trang tình trạng đăng ký
        return RedirectToAction(nameof(RegistrationStatus));


    }
	// Action method để hiển thị thông tin chi tiết khóa đào tạo
	public async Task<IActionResult> Display(int id)
	{
		// Lấy thông tin chi tiết của khóa đào tạo
		var training = await _trainingRepository.GetByIdAsync(id);
        var department = await _deparmentReposity.GetAllAsync();
        if (training == null)
		{
			return NotFound();
		}
		return View(training);
	}
}
