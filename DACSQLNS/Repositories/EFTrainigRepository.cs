﻿using DACSQLNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Repositories
{
    public class EFTrainigRepository :ITrainingRepository
    {
        private readonly ApplicationDbContext _context;
        public EFTrainigRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Training>> GetAllAsync()
        {
            return await _context.Training.Include(t => t.TrainingType).ToListAsync();
        }
        public async Task<Training> GetByIdAsync(int id)
        {
            return await _context.Training.Include(t => t.TrainingType).FirstOrDefaultAsync(t => t.Id == id);
        }
        public async Task AddAsync(Training training)
        {
            _context.Training.Add(training);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Training training)
        {
            _context.Training.Update(training);
            await _context.SaveChangesAsync();
        }
		public async Task DeactivateAsync(int id)
		{
			var training = await _context.Training.FindAsync(id);
			if (training != null)
			{
				training.IsDeleted = false; // Đánh dấu đối tượng là không hoạt động
				await _context.SaveChangesAsync();
			}
		}

	}
}
