﻿using DACSQLNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Repositories
{
    public class EFTrainingTypeRepository:ITrainingTypeRepository
    {
        private readonly ApplicationDbContext _context;
        public EFTrainingTypeRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<TrainingType>> GetAllAsync()
        {
            return await _context.TrainingType.ToListAsync();
           
        }
        public async Task<TrainingType> GetByIdAsync(int id)
        {
            return await _context.TrainingType.FindAsync(id);
        }
        public async Task AddAsync(TrainingType trainingtype)
        {
            _context.TrainingType.Add(trainingtype);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(TrainingType trainingtype)
        {
            _context.TrainingType.Update(trainingtype);
            await _context.SaveChangesAsync();
        }
        public async Task DeactivateAsync(int id)
        {
            var training = await _context.Training.FindAsync(id);
            if (training != null)
            {
                training.IsDeleted = false; // Đánh dấu đối tượng là không hoạt động
                await _context.SaveChangesAsync();
            }
        }
    }
}
