﻿using DACSQLNS.Models;

namespace DACSQLNS.Repositories
{
    public interface IDContractRepository
    {
        Task<IEnumerable<Contract>> GetAllAsync();
        Task<Contract> GetByIdAsync(int id);
        Task AddAsync(Contract contract);
        Task UpdateAsync(Contract contract);
    }
}
