﻿using DACSQLNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Repositories
{
    public class EFDeparmentReposity:IDeparmentReposity
    {
        private readonly ApplicationDbContext _context;
        public  EFDeparmentReposity(ApplicationDbContext context) 
        {
            _context = context;
        }
        public async Task<IEnumerable<Department>> GetAllAsync()
        {
            return await _context.Department.ToListAsync();

        }
        public async Task<Department> GetByIdAsync(int id)
        {
            return await _context.Department.FindAsync(id);
        }
        public async Task AddAsync(Department department)
        {
            _context.Department.Add(department);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Department department)
        {
            _context.Department.Update(department);
            await _context.SaveChangesAsync();
        }
       /* public async Task DeactivateAsync(int id)
        {
            var deparment = await _context.Department.FindAsync(id);
            if (deparment != null)
            {
                deparment.IsDeleted = false; // Đánh dấu đối tượng là không hoạt động
                await _context.SaveChangesAsync();
            }
        }
       */

    }
}
