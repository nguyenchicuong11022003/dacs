﻿using DACSQLNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Repositories
{
    public class EFContractRepository : IDContractRepository
    {
        private readonly ApplicationDbContext _context;
        public EFContractRepository(ApplicationDbContext context)
        {
            _context = context; 
        }
        public async Task AddAsync(Contract contract)
        {
             await _context.AddAsync(contract);
           await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Contract>> GetAllAsync()
        {
            return await _context.Contract.ToListAsync();
        }

        public async Task<Contract> GetByIdAsync(int id)
        {
            return await _context.Contract.FindAsync(id);
        }

        public async Task UpdateAsync(Contract contract)
        {
             _context.Contract.Update(contract);
            await _context.SaveChangesAsync();
        }
    }
}
