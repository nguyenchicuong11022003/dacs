﻿using DACSQLNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DACSQLNS.Repositories
{
    public class EFPayRollRepository : IDPayRollRepository
    {
        private readonly  ApplicationDbContext _context;
        public EFPayRollRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Payroll payroll)
        {
            await _context.AddAsync(payroll);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Payroll>> GetAllAsync()
        {
            return await _context.Payrolls.ToListAsync();
        }

        public async Task<Payroll> GetByIdAsync(int id)
        {
            return await _context.Payrolls.FindAsync(id);
        }

        public async Task UpdateAsync(Payroll payroll)
        {
           _context.Payrolls.Update(payroll);
            await _context.SaveChangesAsync();
        }
    }
}
