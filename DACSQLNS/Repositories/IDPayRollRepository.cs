﻿using DACSQLNS.Models;

namespace DACSQLNS.Repositories
{
    public interface IDPayRollRepository
    {
        Task<IEnumerable<Payroll>> GetAllAsync();
        Task<Payroll> GetByIdAsync(int id);
        Task AddAsync(Payroll payroll);
        Task UpdateAsync(Payroll payroll);
    }
}
