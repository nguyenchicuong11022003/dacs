﻿using DACSQLNS.Models;

namespace DACSQLNS.Repositories
{
    public interface  ITrainingRepository
    {
        Task<IEnumerable<Training>> GetAllAsync();
        Task<Training> GetByIdAsync(int id);
        Task AddAsync(Training training);
        Task UpdateAsync(Training training);
        Task DeactivateAsync(int id);
	}
}
