﻿using DACSQLNS.Models;

namespace DACSQLNS.Repositories
{
    public interface IDeparmentReposity
    {
        Task<IEnumerable<Department>> GetAllAsync();
        Task<Department> GetByIdAsync(int id);
        Task AddAsync(Department department);
        Task UpdateAsync(Department department);
        //Task DeactivateAsync(int id);
    }
}
