﻿using DACSQLNS.Models;

namespace DACSQLNS.Repositories
{
    public interface ITrainingTypeRepository
    {
        Task<IEnumerable<TrainingType>> GetAllAsync();
        Task<TrainingType> GetByIdAsync(int id);
        Task AddAsync(TrainingType trainingType);
        Task UpdateAsync(TrainingType trainingType);
        Task DeactivateAsync(int id);
    }
}
