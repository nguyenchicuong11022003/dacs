﻿using System.ComponentModel.DataAnnotations;

namespace DACSQLNS.Models
{
    public class Department
    {
        public int Id { get; set; }
        [Required]
        public string DepartmentName { get; set; }
        [Required]
        [RegularExpression(@"^\d{10,12}$", ErrorMessage = "Department phone must be a valid 10 to 12-digit number")]
        public string DepartmentPhone { get; set; }
        public bool IsDeleted { get; set; } // Thêm trường này để đánh dấu đối tượng là đã bị xóa
        // Danh sách người dùng trong phòng ban này
        public List<ApplicationUser>? ApplicationUser { get; set; }
    }
}
