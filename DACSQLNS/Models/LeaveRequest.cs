﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class LeaveRequest
    {
        public LeaveRequest() { 
            StartTime = DateTime.Now;
            EndTime = DateTime.Now;
        }
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        [Required]
        [StringLength(1000)]
        public string Reason { get; set; }

        public AbsenceType? Type { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; } = "Đang chờ xử lý";

        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser? ApplicationUser { get; set; }
        // Thêm thuộc tính để lưu đường dẫn của file
        
        public string? ImageUrl { get; set; }
        
        public List<Image>? Images { get; set; }
        [NotMapped]
        public int DaysOff
        {
            get
            {
                return (EndTime.Date - StartTime.Date).Days + 1;
            }
        }
    }

    public enum AbsenceType
    {
        SickLeave, // Nghỉ ốm
        AnnualLeave, // Nghỉ phép hàng năm
        MaternityLeave, // Nghỉ thai sản
        UnpaidLeave, // Nghỉ không lương
        Other // Loại khác
    }
}
