﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class RewardDiscipline
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public DateTime RewardDisciplineDate { get; set; }
        public string RewardDisciplineDescription { get; set; }
         public bool isStatus { get; set; }
        public string UserId { get; set; } // Khóa ngoại đến User
        [ForeignKey("UserId")]
        public ApplicationUser? ApplicationUser { get; set; }
    }
}
