﻿
using System.ComponentModel.DataAnnotations;



namespace DACSQLNS.Models
{
    public class Training
    {
        public int Id { get; set; }
        [Required]
        public string TrainingName { get; set; }
        public string TrainingContent { get; set; }

        [Required(ErrorMessage = "Start time is required")]
        [Display(Name = "Start Time")]
        [DataType(DataType.DateTime)]
        [ValidateDateRange]

        public DateTime TrainingStarTime { get; set; }

        [Required(ErrorMessage = "End time is required")]
        [Display(Name = "End Time")]
        [DataType(DataType.DateTime)]
        [ValidateDateRange]
        public DateTime TrainingEndTime { get; set; }
        public int TrainingTypeId { get; set; }
        public int? DepartmentId { get; set; }
        public bool IsDeleted { get; set; } // Thêm trường này để đánh dấu đối tượng là đã bị xóa
        public TrainingType? TrainingType { get; set; }
        public Department? Department { get; set; }
        public ICollection<UserTraining>? UserTraining { get; set; }

    }
}
