﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class Payroll
    {
        public int Id { get; set; }

        public int ContractId { get; set; }

        public decimal BasicSalary { get; set; }

        public decimal Allowance { get; set; }

        public decimal Insurance { get; set; }

        public decimal Tax { get; set; }

        public decimal TotalSalary { get; set; }

        public int PaidLeaveDays { get; set; }

        public int UnpaidLeaveDays { get; set; }


        public DateTime? DayMonth { get; set; } = DateTime.Now; // Mặc định là ngày hiện tại

        [ForeignKey("ContractId")]
        public Contract Contract { get; set; }
    }
}
