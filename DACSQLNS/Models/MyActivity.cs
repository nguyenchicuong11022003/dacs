﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class MyActivity
    {
        public int Id { get; set; }
        public string ActivityName { get; set; }
        public DateTime ActivityTime { get; set; }
        public string Location { get; set; }
        public string ActivityContent { get; set; } // Chú ý sửa thành ActivityContent
        public string UserId { get; set; } // Khóa ngoại đến User
        [ForeignKey("UserId")]
        public ApplicationUser? ApplicationUser { get; set; }
    }
}
