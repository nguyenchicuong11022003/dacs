﻿using System.ComponentModel.DataAnnotations;

namespace DACSQLNS.Models
{
    public class TrainingType
    {
        public int Id { get; set; }
        [Required]
        public string TrainingTypeName { get; set; }
        public bool IsDeleted { get; set; } // Thêm trường này để đánh dấu đối tượng là đã bị xóa

        public List<Training>? Training { get; set; }
    }
}
