﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{

    public class UserTraining
    {


        public string UserId { get; set; }

        public int TrainingId { get; set; }

        // Sử dụng enum cho Status nếu có tập hợp các giá trị cố định
        public enum TrainingStatus
        {
            ChuaXuLy,
            DaPheDuyet,
            DaTuChoi,

        }

        [Required]
        public TrainingStatus Status { get; set; }

        public bool? Completed { get; set; }


        public DateTime CreatedAt { get; set; }


        public DateTime? CompletedAt { get; set; }

        public Training? Training { get; set; }

        public ApplicationUser? User { get; set; }
    }
}
