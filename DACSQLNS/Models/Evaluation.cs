﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class Evaluation
    {
        public int Id { get; set; }
        public DateTime EvaluationPeriod { get; set; }
        public string EvaluationResult { get; set; }
        public string ManagerAssessment { get; set; }
        public string UserId { get; set; } // Khóa ngoại đến User
        [ForeignKey("UserId")]
        public ApplicationUser? ApplicationUser { get; set; }
    }
}
