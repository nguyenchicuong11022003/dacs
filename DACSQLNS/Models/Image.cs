﻿namespace DACSQLNS.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int LeaveRequestId { get; set; }
        public LeaveRequest? LeaveRequest { get; set; }
    }

}
