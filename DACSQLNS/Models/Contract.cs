﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public decimal ContractSalary { get; set; }
        public string Benefits { get; set; }
        public string Terms { get; set; }

        // Khóa ngoại đến ContractType

        public decimal BasicSalary { get; set; }

        // Khóa ngoại đến ApplicationUser
        [ForeignKey("UserId")]
        [ValidateNever]
        public ApplicationUser? ApplicationUser { get; set; }
        public List<Payroll>? Payrolls { get; set; } // Mối quan hệ nhiều-nhiều với Payroll
    }
}
