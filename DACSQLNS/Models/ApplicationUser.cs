﻿    using Microsoft.AspNetCore.Identity;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            FullName = ""; // Giá trị mặc định cho FullName là chuỗi rỗng
        }

        [Required]
        public string? FullName { get; set; }

        public string? Address { get; set; }
        /*[DataType(DataType.DateTime)]*/

        public DateTime? Birthday { get; set; }
        [NotMapped]
        public int? Age
        {
            get
            {
                if (Birthday != null)
                {
                    var today = DateTime.Today;
                    var age = today.Year - Birthday.Value.Year;
                    if (Birthday > today.AddYears(-age))
                        age--;
                    return age;
                }
                return null;
            }
        }
        public int? DepartmentId { get; set; }
        public Department? Department { get; set; }
        public string? Conpany { get; set; }

        public bool IsDelete { get; set; }
        public string? Gender { get; set; }
        public string? NationalID { get; set; }
        public string? Image { get; set; }
        public string? WorkExperience { get; set; }
        public DateTime? HireDate { get; set; }
        public int SickLeaveDays { get; set; } = 12; // Số ngày nghỉ ốm mặc định trong năm
        public int AnnualLeaveDays { get; set; } = 15; // Số ngày nghỉ phép hàng năm mặc định
        public int MaternityLeaveDays { get; set; } = 180; // Số ngày nghỉ thai sản mặc định
        public ICollection<UserTraining>? UserTraining { get; set; }
        public int NumberOfDay { get; set; }
    }
}
