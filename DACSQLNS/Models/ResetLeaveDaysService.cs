﻿using Microsoft.EntityFrameworkCore;

public class ResetLeaveDaysService : BackgroundService
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public ResetLeaveDaysService(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            var now = DateTime.UtcNow;

            // Kiểm tra xem có phải là ngày đầu tiên của năm mới không
            if (now.Month == 1 && now.Day == 1)
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    // Lấy danh sách người dùng
                    var users = await context.Users.ToListAsync();

                    // Reset số ngày nghỉ phép hàng năm cho mỗi người dùng
                    foreach (var user in users)
                    {
                        user.SickLeaveDays = 12; // Số ngày nghỉ ốm mặc định
                        user.AnnualLeaveDays = 15; // Số ngày nghỉ phép hàng năm mặc định
                        user.MaternityLeaveDays = 180; // Số ngày nghỉ thai sản mặc định
                    }

                    await context.SaveChangesAsync();
                }

                // Chờ đến khi hết ngày 1 tháng 1 mới kiểm tra lại
                var tomorrow = now.AddDays(1).Date;
                var timeToWait = tomorrow - now;
                await Task.Delay(timeToWait, stoppingToken);
            }
            else
            {
                // Nếu không phải ngày 1 tháng 1 thì kiểm tra lại sau 1 ngày
                await Task.Delay(TimeSpan.FromDays(1), stoppingToken);
            }
        }
    }
}
