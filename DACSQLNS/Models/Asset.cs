﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DACSQLNS.Models
{
    public class Asset
    {
        public int Id { get; set; }
        public string AssetName { get; set; }
        public string AssetDescription { get; set; }

        public decimal Value { get; set; }
        public DateTime AssignedDate { get; set; }
        
        public bool isSTatus { get; set; }
        public string UserId { get; set; } // Khóa ngoại đến User
        [ForeignKey("UserId")]
        public ApplicationUser? ApplicationUser { get; set; }
    }
}
