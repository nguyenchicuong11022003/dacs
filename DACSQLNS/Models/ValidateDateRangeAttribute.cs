﻿
using System.ComponentModel.DataAnnotations;

namespace DACSQLNS.Models
{
    // Custom validation attribute để đảm bảo thời gian từ hiện tại trở lên và EndTime lớn hơn StartTime
    public class ValidateDateRangeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var date = (DateTime)value;
            var propertyName = validationContext.MemberName;

            if (propertyName == "TrainingStarTime" && date < DateTime.Now)
            {
                return new ValidationResult("Start time must be greater than or equal to current date");
            }

            if (propertyName == "TrainingEndTime")
            {
                var endDate = (DateTime)value;
                var startDate = (validationContext.ObjectInstance as Training)?.TrainingStarTime;

                if (endDate <= startDate)
                {
                    return new ValidationResult("End time must be greater than Start time");
                }
            }

            return ValidationResult.Success;
        }
    }
}
